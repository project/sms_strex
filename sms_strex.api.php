<?php

/**
 * @file
 * API documentation file for SMS Strex module hooks.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allows modules to alter OutMessage tags.
 *
 * There is no return value for this hook. Modify $sms_tags by reference and not
 * rewrite the $sms.
 *
 * @param array &$sms_tags
 *   A OutMessage tags list.
 * @param \Drupal\sms\Message\SmsMessageInterface $sms
 *   The sms to be sent.
 */
function hook_sms_strex_out_message_tags_alter(array &$sms_tags, \Drupal\sms\Message\SmsMessageInterface $sms) {
  if ($sms->getSender() === 'Test') {
    $sms_tags[] = 'test';
  }
}

/**
 * @} End of "addtogroup hooks".
 */
