<?php

namespace Drupal\sms_strex\Plugin\SmsGateway;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Drupal\sms\Message\SmsDeliveryReport;
use Drupal\sms\Message\SmsMessageInterface;
use Drupal\sms\Message\SmsMessageReportStatus;
use Drupal\sms\Message\SmsMessageResult;
use Drupal\sms\Plugin\SmsGatewayPluginBase;
use Drupal\sms\Plugin\SmsGatewayPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Target365\ApiSdk\ApiClient;
use Target365\ApiSdk\Model\OutMessage;

/**
 * Class StrexGateway.
 *
 * @SmsGateway(
 *   id = "strex",
 *   label = @Translation("Strex"),
 *   outgoing_message_max_recipients = -1,
 * )
 */
class StrexGateway extends SmsGatewayPluginBase implements ContainerFactoryPluginInterface {

  use MessengerTrait;

  const SMS_STREX_GATEWAY_URL = 'https://shared.target365.io';
  const SMS_STREX_TEST_GATEWAY_URL = 'https://test.target365.io';

  /**
   * Settings.
   *
   * @var array
   */
  private $settings;

  /**
   * Sms manager.
   *
   * @var \Drupal\sms\Plugin\SmsGatewayPluginManager
   */
  private $smsManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * StrexGateway constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Settings $settings,
    SmsGatewayPluginManager $sms_manager,
    TimeInterface $time,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->settings = $settings->get('sms_strex');
    $this->smsManager = $sms_manager;
    $this->time = $time;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('settings'),
      $container->get('plugin.manager.sms_gateway'),
      $container->get('datetime.time'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    $default = parent::defaultConfiguration();
    $default += [
      'test_phone' => '',
      'sms_tags' => '',
      'key_name' => '',
      'private_key' => '',
      'sender' => '',
    ];
    return $default;
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    if (!$this->isLiveMode()) {
      $this->messenger()->addWarning($this->t('Test mode is enabled'));
      if (!empty($this->configuration['test_phone'])) {
        $this->messenger()->addWarning($this->t('All sms are rerouted to @phone', ['@phone' => $this->configuration['test_phone']]));
      }
    }

    $form['live_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Live mode'),
      '#default_value' => $this->configuration['live_mode'] ?? FALSE,
    ];

    $form['test_use_log'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use log gateway'),
      '#description' => $this->t('Reroute all sms to log gateway'),
      '#default_value' => $this->configuration['test_use_log'] ?? FALSE,
    ];

    $form['test_phone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test phone number'),
      '#description' => $this->t('Test phone number to reroute all sms'),
      '#default_value' => $this->configuration['test_phone'],
    ];

    $form['sms_tags'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SMS Tags'),
      '#description' => $this->t('SMS tags separated by comma'),
      '#default_value' => $this->configuration['sms_tags'],
    ];

    $form['key_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key name'),
      '#description' => $this->t('The key name'),
      '#default_value' => $this->configuration['key_name'],
    ];

    $form['private_key'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Private key'),
      '#description' => $this->t('The private key.'),
      '#default_value' => $this->configuration['private_key'],
    ];

    $form['sender'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sender name'),
      '#description' => $this->t('A name to display as a sender'),
      '#default_value' => $this->configuration['sender'],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['live_mode'] = $form_state->getValue('live_mode');
    $this->configuration['test_use_log'] = $form_state->getValue('test_use_log');
    $this->configuration['test_phone'] = $form_state->getValue('test_phone');
    $this->configuration['sms_tags'] = $form_state->getValue('sms_tags');
    $this->configuration['key_name'] = $form_state->getValue('key_name');
    $this->configuration['private_key'] = $form_state->getValue('private_key');
    $this->configuration['sender'] = $form_state->getValue('sender');
  }

  /**
   * {@inheritDoc}
   */
  public function send(SmsMessageInterface $sms) {
    $gateway_url = $this->isLiveMode() ? self::SMS_STREX_GATEWAY_URL : self::SMS_STREX_TEST_GATEWAY_URL;
    // Use drupal log for testing.
    if (!$this->isLiveMode() && !empty($this->configuration['test_use_log'])) {
      /** @var \Drupal\sms\Plugin\SmsGatewayPluginInterface $log_gateway */
      $log_gateway = $this->smsManager->createInstance('log');
      if ($log_gateway) {
        return $log_gateway->send($sms);
      }
      return FALSE;
    }

    $sms_tags = [];
    if (!empty($this->configuration['sms_tags'])) {
      $sms_tags = array_map('trim', explode(',', $this->configuration['sms_tags']));
      $sms_tags = array_filter($sms_tags);
    }
    // Allow other modules to alter sms tags.
    $this->moduleHandler->alter('sms_strex_out_message_tags', $sms_tags, $sms);

    $apiClient = new ApiClient($gateway_url, $this->configuration['key_name'], $this->configuration['private_key']);
    $outMessage = new OutMessage();
    $result = new SmsMessageResult();
    foreach ($sms->getRecipients() as $number) {
      $report = (new SmsDeliveryReport())
        ->setRecipient($number)
        ->setStatus(SmsMessageReportStatus::DELIVERED)
        ->setStatusMessage('DELIVERED')
        ->setTimeDelivered($this->time->getRequestTime());
      if (!$this->isLiveMode() && !empty($this->configuration['test_phone'])) {
        $number = $this->configuration['test_phone'];
        $report->setStatusMessage(sprintf('DELIVERED. REROUTED TO %s', $number));
      }
      $result->addReport($report);
      $outMessage
        ->setTransactionId(uniqid((string) time(), TRUE))
        ->setSender($this->configuration['sender'])
        ->setRecipient($number)
        ->setContent($sms->getMessage())
        ->setTags($sms_tags);
    }
    try {
      $apiClient->outMessageResource()->post($outMessage);
    }
    catch (\Throwable $e) {
      $result->setErrorMessage($e->getMessage());
    }
    return $result;
  }

  /**
   * Live mode is enabled.
   */
  private function isLiveMode() {
    return $this->configuration['live_mode'] ?? FALSE;
  }

}
